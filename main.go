package main

import (
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"

	"github.com/gorilla/mux"
)

type Configuration struct {
	Database string
	Logfile  string
}

func main() {

	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)
	log.Println("Startad")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homeLink)

	router.HandleFunc("/officer", officerCreate).Methods("POST")
	router.HandleFunc("/officer", officersGetAll).Methods("GET")
	router.HandleFunc("/officer/{id}", officerGetOne).Methods("GET")
	router.HandleFunc("/officer/{id}/update", officerUpdate).Methods("POST")
	router.HandleFunc("/officer/{id}", officerDelete).Methods("DELETE")

	router.HandleFunc("/case", caseCreate).Methods("POST")
	router.HandleFunc("/case", caseGetAll).Methods("GET")
	router.HandleFunc("/case/{id}", caseGetOne).Methods("GET")
	router.HandleFunc("/case/solved/{id}", caseSolved).Methods("POST")

	log.Fatal(http.ListenAndServe(":8080", router))
}
