# A first dip into the Golang pool

An API to manage stolen bikes, made a test. Real life use is not recommended.


## Install 
- git clone https://gitlab.com/chinprivat/doktorct01.git
- cd doktorct01

### Non docker
- go mod install
- go build
- ./doctorct01

### Docker

- docker build --tag doktorct01 .
- docker run --publish 8080:8080 doktorct01

Acces the API through 127.0.0.1:8080

Default officer and sheriff is Lucky Luke, username ll (lowercase LL) password horse

## Usage

(very) Simple auth is used. Send header "username" and "password" 

You can add, delete or update officers. You can also mark cases as solved. When things change cases are assigned new officers 10 at the time.

## Test

Run go test -v to run the tests. They access the database directly to set up the environment so they need to be started inside the docker container or in a non docker envirinment.


# API
### Officers
An officer can be Sheriff or not. Sheriffs have more rights than others, they are able to edit officers. That includes locking themselfes out.

#### POST /officer  
Only sheriffs can do this

Create officer, body shuold contain json-text:


	  {
		"Officername": "Name",
		"Busy": 0,
		"IsSheriff": 0,
		"UserName": "username",
		"PassWord": "a secure pw",
		"SolvedCases": 0
	  }


#### GET /officer  

Get all offiers
Only officers can do this



#### GET /officer/{id}  

Get one officer
Only officers can do this

#### POST /officer/{id}/update 
Update officer
Only sheriffs can do this


#### DELETE /officer/{id}  
Delete one officer
Only sheriffs can do this


### Cases

#### POST /case
Create case
Publicly available

Body should contain json-text:

		  {
			"CaseTitle": "En stulen jolle",
			"CaseDesc": "Snedseglad såklart"
		  }

#### GET /case
Get all cases

#### GET /case/{id}
Get one case

#### POST /case/solved/{id}
Mark a case as solved.
Only officers can do this

