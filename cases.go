package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

/* Functions to manage caes */

func caseCreate(w http.ResponseWriter, r *http.Request) {
	var newCase BikeCase
	reqBody, _ := ioutil.ReadAll(r.Body)

	json.Unmarshal(reqBody, &newCase)

	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)

	stmt, err := db.Prepare("INSERT INTO cases (caseTitle, caseDesc, status) VALUES (?,?,0);")
	res, err := stmt.Exec(newCase.CaseTitle, newCase.CaseDesc)
	chkErr(err)

	id, _ := res.LastInsertId()

	db.Close()

	assignCases()

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, statusJson(201, "Case created "+strconv.Itoa(int(id))))

}

func caseSolved(w http.ResponseWriter, r *http.Request) {
	var currentCase BikeCase
	if authUser(r.Header, false) == true {
		caseId := mux.Vars(r)["id"]
		currentCase = getCaseData(caseId)
		if currentCase.Status == 0 {

			db, err := sql.Open("sqlite3", "./doktor.sqlite")
			chkErr(err)
			stmtCase, err := db.Prepare("update cases set status = 1 where cid = ?;")
			resCase, err := stmtCase.Exec(caseId)
			_, err = resCase.RowsAffected()
			chkErr(err)

			stmtOf, err := db.Prepare("update officers set solvedCases = solvedCases + 1 where oid = ?;")
			resOf, err := stmtOf.Exec(currentCase.AssignedOfficer)
			_, err = resOf.RowsAffected()

			assignCases()

		}
	}
}

func assignCases() {
	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)
	rows, err := db.Query("select cid from cases where assignedOfficer is null order by cid limit 10;")
	chkErr(err)

	var unsolvedCase int
	var unsolvedCases [10]int
	var sqlString string
	slask := 0

	for rows.Next() {
		err = rows.Scan(&unsolvedCase)
		unsolvedCases[slask] = unsolvedCase
		slask = slask + 1

		chkErr(err)
	}
	rows.Close()

	for i := 0; i < slask; i++ {

		sqlString = "update cases set assignedOfficer = (select oid from officers where oid not in (select assignedOfficer from cases where assignedOfficer is not null and status = 0) order by solvedCases limit 1) where cid=" + strconv.Itoa(unsolvedCases[i]) + ";"

		stmt, err := db.Prepare(sqlString)
		_, err = stmt.Exec()
		// affect, err := res.RowsAffected()
		// fmt.Println(strconv.Itoa(int(affect)))
		chkErr(err)
	}

	db.Close()

}

func caseGetAll(w http.ResponseWriter, r *http.Request) {

	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)
	rows, err := db.Query("select c.cid, c.caseTitle, c.caseDesc, COALESCE(o.officername, 'Some one else'), c.status from cases c left join officers o on  o.oid = c.assignedOfficer;")
	chkErr(err)

	// I know this is and odd way....
	var o [150]CaseList

	// var slask int
	slask := 0
	isLoggedIn := false

	if authUser(r.Header, false) == true {
		isLoggedIn = true
	}

	for rows.Next() {

		err = rows.Scan(&o[slask].Cid, &o[slask].CaseTitle, &o[slask].CaseDesc, &o[slask].Officername, &o[slask].Status)

		if isLoggedIn == false {
			o[slask].Officername = "Not official unless you are a police officer"
		}
		slask = slask + 1
		chkErr(err)

	}
	var slice = o[0:slask]
	data, _ := json.Marshal(slice)

	rows.Close()
	db.Close()

	fmt.Fprintf(w, string(data))

}

func getCaseData(caseId string) BikeCase {

	var o BikeCase

	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)

	rows := db.QueryRow("select * from cases where cid = $1", caseId)

	err = rows.Scan(&o.Cid, &o.AssignedOfficer, &o.CaseTitle, &o.CaseDesc, &o.Status)
	switch err {
	case nil:
		return o
	case sql.ErrNoRows:

	default:
	}

	db.Close()

	return o
}

func caseGetOne(w http.ResponseWriter, r *http.Request) {
	caseId := mux.Vars(r)["id"]
	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)

	rows := db.QueryRow("select * from cases where cid = $1", caseId)
	// chkErr(err)
	var o BikeCase
	var m statusMsg
	data, _ := json.Marshal("")

	err = rows.Scan(&o.Cid, &o.AssignedOfficer, &o.CaseTitle, &o.CaseDesc, &o.Status)
	switch err {
	case nil:

		if authUser(r.Header, false) == false {
			o.AssignedOfficer = 0
		}
		data, _ = json.Marshal(o)
	case sql.ErrNoRows:
		m.StatusCode = 404
		m.StatusMsg = "There is no such case"
		data, _ = json.Marshal(m)
		w.WriteHeader(http.StatusNotFound)

	default:
		m.StatusCode = 500
		m.StatusMsg = "Server error"
		data, _ = json.Marshal(m)
		w.WriteHeader(http.StatusInternalServerError)
	}
	db.Close()

	// chkErr(err)
	fmt.Fprintf(w, string(data))
}
