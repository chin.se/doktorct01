package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

/* Support functions */

func chkErr(err error) {
	if err != nil {
		log.Println(err)
		panic(err)
	}
}

type Officer struct {
	Oid         int
	Officername string
	Busy        int
	IsSheriff   int
	UserName    string
	PassWord    string
	SolvedCases int
}

type BikeCase struct {
	Cid             int
	AssignedOfficer int
	CaseTitle       string
	CaseDesc        string
	Status          int
}

type CaseList struct {
	Cid         int
	CaseTitle   string
	CaseDesc    string
	Officername string
	Status      int
}

type statusMsg struct {
	StatusCode int64
	StatusMsg  string
}

func statusJson(statusCode int64, statusString string) string {

	var m statusMsg
	var data []uint8
	m.StatusCode = statusCode
	m.StatusMsg = statusString
	data, _ = json.Marshal(m)

	return string(data)

}

func authUser(h http.Header, adminReq bool) bool {
	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)

	var u string = ""
	var p string = ""

	for indexet, b := range h {
		if indexet == "Username" {
			u = strings.Join(b, "")
		}
		if indexet == "Password" {
			p = strings.Join(b, "")
		}
	}

	if u == "" || p == "" {

		return false

	}

	rows := db.QueryRow("SELECT * from officers where userName = $1 AND passWord = $2", u, p)
	chkErr(err)

	var o Officer

	switch err := rows.Scan(&o.Oid, &o.Officername, &o.Busy, &o.IsSheriff, &o.UserName, &o.PassWord, &o.SolvedCases); err {
	case sql.ErrNoRows:
		return false
	case nil:
		if adminReq == true {
			if o.IsSheriff == 1 {
				return true
			} else {
				return false
			}
		}
		return true
	default:
		panic(err)
		return false
	}

	db.Close()

	return false

}

func isOfficerValid(name string, username string) bool {
	db, err := sql.Open("sqlite3", "./doktor.sqlite")
	chkErr(err)
	rows := db.QueryRow("SELECT * from officers where Officername = $1 OR userName = $2", name, username)
	chkErr(err)

	var o Officer

	switch err := rows.Scan(&o.Oid, &o.Officername, &o.Busy, &o.IsSheriff, &o.UserName, &o.PassWord, &o.SolvedCases); err {
	case sql.ErrNoRows:
		return true
	case nil:
		return false
	default:
		panic(err)
		return false
	}

	db.Close()

	return false

}
