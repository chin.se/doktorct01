package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	_ "github.com/mattn/go-sqlite3"

	"github.com/gorilla/mux"
)

/*
	Functions to handle officers
*/

func homeLink(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Local police is docker")
}

func officerDelete(w http.ResponseWriter, r *http.Request) {

	if authUser(r.Header, true) == true {

		officerId := mux.Vars(r)["id"]
		db, err := sql.Open("sqlite3", "./doktor.sqlite")
		chkErr(err)
		stmt, err := db.Prepare("DELETE FROM officers where oid=?;")
		res, err := stmt.Exec(officerId)
		affect, err := res.RowsAffected()
		chkErr(err)
		fmt.Fprintf(w, strconv.Itoa(int(affect)))

		stmt, err = db.Prepare("UPDATE cases set assignedOfficer = null where assignedOfficer=? and status = 0;")
		res, err = stmt.Exec(officerId)
		affect, err = res.RowsAffected()
		chkErr(err)
		fmt.Fprintf(w, strconv.Itoa(int(affect)))

	} else {
		w.WriteHeader(http.StatusForbidden)

		fmt.Fprintf(w, statusJson(401, "Forbidden"))
	}
}

func officerCreate(w http.ResponseWriter, r *http.Request) {
	var newOfficer Officer
	reqBody, _ := ioutil.ReadAll(r.Body)

	if authUser(r.Header, true) == true {
		json.Unmarshal(reqBody, &newOfficer)

		if isOfficerValid(newOfficer.Officername, newOfficer.UserName) == true {

			db, err := sql.Open("sqlite3", "./doktor.sqlite")
			chkErr(err)

			stmt, err := db.Prepare("INSERT INTO officers (officername, busy, isSheriff, userName, passWord, solvedCases) VALUES (?,?,?,?,?,?);")
			res, err := stmt.Exec(newOfficer.Officername, newOfficer.Busy, newOfficer.IsSheriff, newOfficer.UserName, newOfficer.PassWord, newOfficer.SolvedCases)
			chkErr(err)
			id, _ := res.LastInsertId()

			assignCases()

			w.WriteHeader(http.StatusCreated)
			fmt.Fprintf(w, statusJson(201, "Officer created "+strconv.Itoa(int(id))))

		} else {
			w.WriteHeader(http.StatusConflict)
			fmt.Fprintf(w, statusJson(409, "Officer exists"))

		}

	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, statusJson(401, "Forbidden"))
	}

}

func officerUpdate(w http.ResponseWriter, r *http.Request) {
	var newOfficer Officer
	var officerId int
	reqBody, _ := ioutil.ReadAll(r.Body)

	if authUser(r.Header, true) == true {
		json.Unmarshal(reqBody, &newOfficer)

		if i, err := strconv.Atoi(mux.Vars(r)["id"]); err == nil {
			officerId = i

		}

		if officerId == newOfficer.Oid {
			db, err := sql.Open("sqlite3", "./doktor.sqlite")
			chkErr(err)

			stmt, err := db.Prepare("UPDATE officers set officername = ?, busy = ?, isSheriff = ?, userName = ?, passWord = ?, solvedCases = ? where oid = ?;")
			_, err = stmt.Exec(newOfficer.Officername, newOfficer.Busy, newOfficer.IsSheriff, newOfficer.UserName, newOfficer.PassWord, newOfficer.SolvedCases, newOfficer.Oid)
			chkErr(err)

			w.WriteHeader(http.StatusAccepted)
			fmt.Fprintf(w, statusJson(202, "Officer updated"))

		} else {

			w.WriteHeader(http.StatusConflict)
			fmt.Fprintf(w, statusJson(409, "Conflicting Officers"))

		}

	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, statusJson(401, "Not authorized"))
	}
}

func officersGetAll(w http.ResponseWriter, r *http.Request) {
	var data []uint8
	if authUser(r.Header, false) == true {
		db, err := sql.Open("sqlite3", "./doktor.sqlite")
		chkErr(err)
		rows, err := db.Query("SELECT * FROM officers;")
		chkErr(err)

		// I know this is and odd way....
		var o [150]Officer

		// var slask int
		slask := 0

		for rows.Next() {
			err = rows.Scan(&o[slask].Oid, &o[slask].Officername, &o[slask].Busy, &o[slask].IsSheriff, &o[slask].UserName, &o[slask].PassWord, &o[slask].SolvedCases)
			slask = slask + 1
			chkErr(err)

		}
		var slice = o[0:slask]

		data, _ = json.Marshal(slice)

		rows.Close()
		db.Close()

		fmt.Fprintf(w, string(data))
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, statusJson(401, "This is not allowed, go to the Police Academy first"))
	}
}

func officerGetOne(w http.ResponseWriter, r *http.Request) {

	if authUser(r.Header, false) == true {

		officerId := mux.Vars(r)["id"]
		db, err := sql.Open("sqlite3", "./doktor.sqlite")
		chkErr(err)

		rows := db.QueryRow("SELECT * from officers where oid = $1", officerId)
		// chkErr(err)
		var o Officer
		data, _ := json.Marshal("")

		err = rows.Scan(&o.Oid, &o.Officername, &o.Busy, &o.IsSheriff, &o.UserName, &o.PassWord, &o.SolvedCases)
		switch err {
		case nil:
			data, _ = json.Marshal(o)
		case sql.ErrNoRows:
			fmt.Fprintf(w, statusJson(404, "There is no such cop"))
			w.WriteHeader(http.StatusNotFound)

		default:
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, statusJson(500, "Server Error"))
		}

		// chkErr(err)
		fmt.Fprintf(w, string(data))
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, statusJson(401, "Forbidden"))

	}

}
