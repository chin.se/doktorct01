package main

import (
	"bytes"
	"database/sql"
	"net/http"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {

	expected := true
	recieved := cleanDB()
	assert.Equal(t, expected, recieved, "Init Database")

}

func cleanDB() bool {
	db, err := sql.Open("sqlite3", "./doktor.sqlite")

	stmt, err := db.Prepare("DELETE FROM officers;")
	_, err = stmt.Exec()
	if err != nil {
		return false
	}

	stmt, err = db.Prepare("DELETE FROM cases;")
	_, err = stmt.Exec()
	if err != nil {
		return false
	}

	stmt, err = db.Prepare("update sqlite_sequence set seq = 0;")
	_, err = stmt.Exec()
	if err != nil {
		return false
	}

	stmt, err = db.Prepare("insert into officers (officername, busy, isSheriff, username, password, solvedCases) values ('Luky Luke', 0, 1, 'll', 'horse', 0);")
	_, err = stmt.Exec()
	if err != nil {
		return false
	}

	return true

}

func TestStatusJson(t *testing.T) {
	expected := "{\"StatusCode\":200,\"StatusMsg\":\"Det funkar\"}"
	received := statusJson(200, "Det funkar")
	assert.Equal(t, expected, received)
}

func TestIsOfficerValidValid(t *testing.T) {
	expected := true
	recieved := isOfficerValid("TEST", "TEST")
	assert.Equal(t, expected, recieved)
}

func TestIsOfficerValidInvalid(t *testing.T) {
	expected := true
	recieved := isOfficerValid("ll", "TEST")
	assert.Equal(t, expected, recieved)
}

func TestCreateOfficer(t *testing.T) {

	dat, err := os.ReadFile("./testdata/no.txt")

	client := &http.Client{}

	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/officer", bytes.NewBuffer(dat))
	// ...
	req.Header.Add("username", "ll")
	req.Header.Add("password", "horse")

	response, err := client.Do(req)

	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusCreated
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestCreateDuplicateOfficer(t *testing.T) {

	dat, err := os.ReadFile("./testdata/no.txt")

	client := &http.Client{}

	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/officer", bytes.NewBuffer(dat))
	// ...
	req.Header.Add("username", "ll")
	req.Header.Add("password", "horse")

	response, err := client.Do(req)

	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusConflict
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestCreateCaseAsPublic(t *testing.T) {

	client := &http.Client{}

	dat, err := os.ReadFile("./testdata/c1.txt")
	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/case", bytes.NewBuffer(dat))
	response, err := client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	dat, err = os.ReadFile("./testdata/c2.txt")
	req, _ = http.NewRequest("POST", "http://127.0.0.1:8080/case", bytes.NewBuffer(dat))
	response, err = client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	dat, err = os.ReadFile("./testdata/c3.txt")
	req, _ = http.NewRequest("POST", "http://127.0.0.1:8080/case", bytes.NewBuffer(dat))
	response, err = client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusCreated
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestCreateCaseAsOfficer(t *testing.T) {

	client := &http.Client{}
	dat, err := os.ReadFile("./testdata/c4.txt")
	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/case", bytes.NewBuffer(dat))
	req.Header.Add("username", "ll")
	req.Header.Add("password", "horse")
	response, err := client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusCreated
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestSolve2(t *testing.T) {

	client := &http.Client{}

	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/case/solved/2", nil)
	req.Header.Add("username", "ll")
	req.Header.Add("password", "horse")
	response, err := client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusOK
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestSolve1(t *testing.T) {

	client := &http.Client{}

	req, _ := http.NewRequest("POST", "http://127.0.0.1:8080/case/solved/1", nil)
	req.Header.Add("username", "ll")
	req.Header.Add("password", "horse")
	response, err := client.Do(req)
	if err != nil {
		assert.NoError(t, err, "Unknown error")
	}

	expected := http.StatusOK
	recieved := response.StatusCode
	assert.Equal(t, expected, recieved)
}

func TestResetData(t *testing.T) {

	expected := true
	recieved := cleanDB()
	assert.Equal(t, expected, recieved, "Restore Database")

}
